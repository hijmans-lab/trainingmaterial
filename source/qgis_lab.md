# Analyze sUAS Data with QGIS

Sierra Mabanta, Emily Hurry, Arielle Rose & Alex Mandel

[Center for Spatial Sciences](https://spatial.ucdavis.edu/)
[University of California, Davis](https://www.ucdavis.edu)

-----

This guide will familiarize you with digitizing and exporting data using information collected in the field

-----

## Getting Started [QGIS](https://www.qgis.org/en/site/)

1. Open the QGIS. If you don't have it installed yet you can get it from <https://www.qgis.org/en/site/>
1. Once the program is open, select the white paper icon on the top left of the page to start a new project.
![Folder icon](images/qgis/image3.png)

## Add a Basemap

*You do not have to work with a basemap but this may be useful information to have. If you would like to work with Google Maps, OpenStreetMap or other pre-made basemaps.*

1. Select "Plugins" and then "manage plugins" located on the top menu bar towards the center of the default options.
1. Under the icon, ![All icon](images/qgis/image1.png) , select the box on the left of the page ![Quickmapservicesicon](images/qgis/image8.png) and select "Install Plugin"
1. Once installed, close out the window and select "Web" and then QuickMapServices again.
1. Select "Settings" and under the last tab that says “more services” select “Add contributed packs” >ok>save.

Now that you have the option available to you, you can add a basemap by selecting “Web” and then “QuickMapServices”. Pick one that works for you and it will be added to your layers. Your layers should align clearly.

## Loading data in QGIS
<https://docs.qgis.org/2.18/en/docs/user_manual/introduction/getting_started.html#sample-session-load-raster-and-vector-layers>

Unmanned Aircraft Systems (UAS) data is usually a Raster, most commonly a Geo-Tiff (.tif). Drag and drop, use the browse tool, or the raster load tool to load a UAS flight into the map.

## Digitizing
Digitizing is the process of converting raster GIS data into vector data by manually tracing the desired features. This can be done using the polygon tool in QGIS. The goal is to trace individual plots from the field that you would like to extract data from. Some of the plots are 1 acre and some are smaller plots within an acre plot. It will be up to you to decide the best polygon configuration to gain the information desired.

### Drawing Polygons

1. First we are going to add a new layer. Go to the top menu bar and select “Layer” then “Create Layer”  >  “New Shapefile Layer...”
1. A window will pop up. Select “polygon”. Make sure projection is is “EPSG: 32610”. Click “OK” at the bottom of the window.
1. A new window will pop up with the option to save this layer. Select the path that you would like to work from and name the polygon layer appropriately. Example “West_Plot_May2018_Field1_Boundary”. When you are done select “Okay”.
1. Double check that your project is still in the correct projection at the bottom right corner. Change if necessary to “EPSG: 32610”.
    >Pro Tip: For this specific project at Russell Ranch we are choosing the projection EPSG: 32610 because the flight is contained within this CA zone (UTM zone 10). This is typical for UAV flights to be in a specific projection because the area is relatively small.

1. To begin digitizing. Select the layer that you just created in the “Layers Panel” located in the bottom left corner by single clicking on it. This will highlight the layer. (It may already be highlighted.)
1. In the toolbar area select the single pencil  ![pencilicon](images/qgis/image29.png) This will allow you to begin editing. You will know that it is selected when some of the greyed out option show in color meaning they are ready to use. You can toggle the edit button as necessary.
1. Select “Add a Feature” button ![greenshape](images/qgis/image9.png)

    > Pro Tip: You can navigate your map using your arrow keys. This may save you time from switching your pointer to a dragging function or having to zoom in and out to arrange the image while you are in the process of digitizing.

1. Create your polygon by left clicking where you want your boundary. The next point in your polygon will be connected with a thin red dashed line. You can close your polygon by right clicking anywhere. A window will pop up that says ID with a “NULL” text. Either enter your own ID or click okay. This entry will become the attribute table.

1. You can continue adding polygons but understand that they will all be under the same layer.
![polygonsononelayer](images/qgis/image14.png)

### Moving Polygons

1. Select “Move Feature(s)” tool ![greenshapewithanarrow](images/qgis/image33.png) . This tool will display your mouse pointer as large crosshairs without a circle.
1. You can left click and drag on any polygon to move it.

### Edit the Shape of an existing polygon

1. Select “Node Tool” ![circlenode](images/qgis/image16.png). These crosshairs will look the same as the “move feature(s)” one.
1. Select the polygon you want to edit with a single left click.
1. You can drag points that you already created to the desired location.
1. You can also add a new point by double left clicking on the desired line. A new point will be added and you can move this to the desired location.  

### Raster Ovals Digitizing

If you would like to create a polygon that is exactly a rectangle or an oval you can turn on this plugin.

1. Go to “Plugins” in the top menu tabs and select “manage and install plugins...”
1. Find “Rectangle Ovals Digitizing” Install plugin. ![pluginsscreenshor](images/qgis/image21.png)
1. Have fun and explore these tool options ![options](images/qgis/image2.png)

    > Note: Ensure editing is on to enable these functions. To do this, right-click on your polygon layer and select “toggle editing” ![toggleeditingicon](images/qgis/image30.png)

1. To use the rotate function ![rotatefunction](images/qgis/image13.png) or delete, cut, copy, and paste ![cutcopypasteicon](images/qgis/image10.png) you will need to select your polygon feature using “select feature using area or single click” ![yellowsquaresymbol](images/qgis/image20.png).

## Data Analysis - Data Extraction from Raster

### Zonal Statistics

Zonal Statistics is one technique for analysing your data. This tool will give you the chosen statistics for each individual polygon that
you created within the selected layer.
*These instructions are for QGIS 2.x, QGIS 3 instructions coming soon.*

1. From the QGIS Desktop, select “Plugins” from the top of the menu bar, then “Manage and Install Plugins…” in the drop-down menu ![pluginpicture](images/qgis/image15.png)
1. A pop-up should appear, type in “Zonal Statistics” in the search bar on top. The plugin should appear, select the box to the left and ensure a check appears in the box to enable the plug-in.
1. Return to the main QGIS Desktop screen and select “Raster” at the top of the screen, follow the drop-down screen to “Zonal statistics”, select “zonal statistics”.
![Zonalstatisticssymbol](images/qgis/image7.png))
1. Your raster layer should automatically be on the layer you created for the polygons, if it is not you can change it now.
1. Select the band you would like to do the analysis for.

    >Pro Tip: This is a good time to know what camera you used to obtain this image and which band is being represented in the dropdown. There is a quick reference at the bottom of this section (IV. Data Analysis).

1. Put in an “Output column prefix” that makes sense to you. Ex. “B1” if “Band 1” is selected.
1. Check boxes of the statistics you would like and select “OK”.
1. Select the “Identify feature” ![indentifyfeatureicon](images/qgis/image28.png) on the top of the page.
1. Select a polygon. You should see the statistical information displayed on the right hand side in the toolbar called “Identify Results”.

### Raster Calculator

![Openrastercalculatorpicture](images/qgis/image25.png)

1. Click on “Raster” on the top of the page and select “Raster calculator” ![outputlayericon](images/qgis/image11.png). Click on the Output Layer on the top right of the menu and save your file where you choose to put it and then click “Save”.
1. Under “Raster bands”, double click on the Raster band you are working with to move it down to your “Raster calculator expression” tile.
1. When entering your calculation, ensure your calculation is within the quotation marks or it will be labeled invalid.

**Note: you must perform each of these functions separately.**
```
#General Crop Health
(NIR - R) / (NIR+R) = NDVI

#Yield Estimation
(NIR/G)-1 = GCVI

#Nitrogen Deficiency
(NIR - RE) / (NIR+RE) = NDRE
```

Red Edge bands:
1. Blue
2. Green
3. Red
4. RedEdge
5. Near Infrared (NIR)


Sequoia bands:
1. Green (550 BP 40)
2. Red (660 BP 40)
3. Red Edge (735 BP 10)
4. Near infrared (790 BP 40)

![CalculatingNDVIonaredegde](images/qgis/image4.png)
![LayerspanelNDVICalc](images/qgis/image24.png)

### Filter Image Based on Value

We are now going to use the values we gained from doing the raster calculations to set parameters on the image creating a filter. Each pixel has a data value (which we all know) but we can use this information to our advantage. For this class we are interested in looking at plants so lets use NDVI to set our filter parameters. What value did you get for NDVI when you did your raster calculations? We will create our filter by setting (Filter “true” if > minimum NDVI value of plants). The output will result in pixels with plants being assigned a “1” for true and a “0” for false for pixels without plants. Then we take that filter and multiply it by the original values(pixel for pixel)! You guessed it! The output result is plant-pixels with data and non-plant-pixels with a 0! It’s cool if it doesn’t make sense yet, let’s run through these steps together!

1. The goal is to identify all pixels that are not plants, so we can remove or zero out their values. Lets say your NDRE value is 0.25. Open your raster calculator and enter a name for the new output layer.
1. Double click on the band name so that it appears in the lower “Raster expression” and complete the expression by writing the value >= “.25”.

    ![Rastercalculatorexpression](images/qgis/image26.png)

1. Click OK to create the new raster and see your new layer.

    ![NDVIcalculationpic](images/qgis/image5.png)

## Visualization

### False Color Infrared
Plants reflect red and near infrared wavelengths, so changing the band order to visualize this reflectance can make it easier to identify vegetation. The band order for False Color is **NIR, R, G**.

  1. Open up properties for the multispectral RR data and go to the style band
  1. Under Band Rendering change the Red band to band 5, the Green Band to Band 3, and the Blue Band to Band 2 (for red-edge data).
  1. Click Apply and okay

### Create Map

1. Open Project->Composer Manager
1. Click Add and write the title of your map and click okay. A new blank map will open.
1. Click the Add New Map icon ![Newmapicon](images/qgis/image12.png) , near the middle of the left hand toolbar. Click and drag the box to cover the page. This will insert your map onto the canvas
1. Click the move map icon ![Movemapicon](images/qgis/image6.png)  to reposition and scale your map, be careful of lag.  If you want to move the map atop the canvas use the select item tool ![imageselectionpic](images/qgis/image20.png) and drag the white corners
1. Once you have positioned the map the way you like, go ahead and add a title ![addtitleicon](images/qgis/image22.png) by clicking the icon on the left toolbar.
1. On the right hand side you will see an Item Properties Tab. In this tab you can set the text, font, size, color, and other properties of your title.
    ![Itemproperties](images/qgis/image19.png)
1. To add a legend click the icon ![legendicon](images/qgis/image31.png) and drag a box. The legend will auto fill with the items in the legend in your project. You can edit the properties of the legend the same way you can edit those of the title.
1. Double click on a legend item to change its name
1. Remove an item by selecting the minus icon
1. Add scale ![scalesymbol](images/qgis/image27.png) by clicking the scale icon
1. Add north arrow by adding image ![imagesymbol](images/qgis/image17.png) . Then expand search directory under item properties. Scroll through the icons to select an appropriate arrow.

    ![imagesearchdirectories](images/qgis/image23.png)

    > Note: If you need assistance finding your north arrow, follow these instructions: http://www.qgistutorials.com/en/docs/making_a_map.html

1. The export buttons ![Exportbuttons](images/qgis/image32.png)  are near the top left of the page. You can select to print, export as image, as SVG, or as PDF.
