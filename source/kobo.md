# KoBo Toolbox for Geographic Data Collection

Sierra Mabanta, Emily Hurry, Arielle Rose & Alex Mandel

[Geospatial and Farming Systems Research Consortium University of California, Davis](https://gfc.ucdavis.edu)

-----

This guide helps you to:

* Create and Setup a KoBo Collect Account
* Use Geo-type Questions
  * Download forms from an account
  * Import CSV files to QGIS
* Collect Data with KoBoCollect using an Android Device
  * Upload finalized data
  * Review Data Online
  * Upload Data to a Computer Without Wifi
* Misc information
  * View GPS Points
  * Open Data
  * Open Data on an Android Device
  * Review Data on a Computer without Internet

What you’ll need:

* An Android tablet or phone and a computer
* [KoBoCollect](https://play.google.com/store/apps/details?id=org.koboc.collect.android) on mobile device or use [KoBoToolbox](http://www.kobotoolbox.org) on a computer.
* An Kobo account

----

## Create and Setup a KoBoCollect Account

KoboToolbox [website](http://www.kobotoolbox.org)
1. Scroll down on the page until you reach “Create An account or Log In”. Be cure to select the option “RESEARCHERS, AID WORKERS & EVERYONE ELSE” and select “Sign Up of Log in” to create an account with Kobo toolbox.
2.  After you create an account follow the activation link through an email. You will be prompted to a new page.
![Home page](images/kobo/image28.png)
3. It’s time to create your first form. You will be prompted to a new page after creating an account. Click on “New” on the top left corner of the page. Give the project a name, description, sector, and country. Select “create project”.
![Create project](images/kobo/image39.png)
![Exaple project](images/kobo/image46.png)
4. A pop-up will appear, select “Design in Form Builder”.
![Step two, project creation](images/kobo/image25.png)

## Geo-Type Questions

1. Click on the plus sign on the left side of the page. Type in a question in the search bar and click “add question”. If the question is a geography question, select Point, Line or Area. You may also select Text if you have a metadata question.  
![question line](images/kobo/image48.png)
Here are the different Types of Geo Responses you can Use:
![Question: How to get to bathroom? Drop down menu select one: suggested selection - point, line, area.](images/kobo/image26.png)
2. Once you type in a question and a preferred response type. The symbol will be to the left of the new question.
![point type selected](images/kobo/image15.png)
3. To evaluate options for the question, select the settings symbol to the right of the question. Here you can edit the name, add a hint, tag, and use a default response. You can also select “Skip Logic” to display the question only when other conditions apply. For example:
Q1: Are you currently in school?
Q2: What directions did you take to get to school this afternoon?
![skip logic selected](images/kobo/image21.png)
4. Notice the “Appearance (Advanced)” option on the bottom of the screen. These options will allow you to change the appearance of questions on the form. Below is a table of possible appearance attributes and how questions appear in the form.
→ arrows point to geo-type questions.
![Arrows point to multiline and draw.](images/kobo/image57.png)
![data column line drop down](images/kobo/image32.png)
Here are some examples:
![Q: Which path do hikers observe A. thaliana the most often? Arrow pointing to multiline](images/kobo/image53.png)
![Q: Find the area of the polygon where A. thaliana is grown.](images/kobo/image3.png)
Note: If you are asking a question with discrete answers or options, you may select the icon for “Select One” ![target symbol](images/kobo/image44.png) and insert the answers.
![Q: is it raining? "yes" or "no"](images/kobo/image29.png)
5. Save the project and return to the main menu.
![start time and stop time selected](images/kobo/image34.png)
6. Click Preview Form ( The eye icon near the top of the page) to quickly view the questions and test skip logic.
![world map, form preview.](images/kobo/image20.png)
7. Scroll to the bottom of the page and select “Validate”. A popup will appear, press close. Close the Form Preview and save the form by clicking the Create/Save button on , then 'X' to close the form.
![save button top right](images/kobo/image23.png)
8. To start collecting data you need to first deploy the draft form as a new data collection project.
![deploy](images/kobo/image22.png)
9. If you would like to return to the main menu, click the save button at the top right of the screen and then the green icon on the top left of the screen to return to your list of projects.
10. In order to input this data, select DEPLOY under the project form. After this, you must select the form of data collection under the Collect Data segment. In this case, we are using an Andriod application  


## Download Data as a CSV File

1. Download data in CSV format as soon as any data has been collected. After you have sent the finalized forms, navigate to the Settings menu and select “Projects” and click on the project you wish to download as a CSV file.
![project](images/kobo/image55.png)
2.  Select the project name and scroll to the bottom of the page. Under “Data”, select “Downloads”.
![download](images/kobo/image35.png)
3. Under “Select Export Type”, click on “CSV (legacy)” in the drop-down menu. You may also change the Value and header format to XML values and headers.
![CSV](images/kobo/image12.png)
4. Click on “Advanced Export” and then “Create Export”.
![Advanced export](images/kobo/image51.png)
5. Select the links underneath “Filename”. The data should download as an excel file. Open the data in excel. Each question should have a column. With the answer in the cell below.
![excel sheet](images/kobo/image37.png)
6. This CSV file turns the data into a text file which can be labeled as an x and y points. These points can be uploaded in a table to QGIS which will plot the data for you.


### Import CSV files to QGIS

1. Open QGIS. Click on "Layers" and then "Add Delimited Text Layer".
![Add delimited Text Layer](images/kobo/image1.png)
2. In the Create a Layer from a Delimited Text File dialog, click on "Browse" to specify the path of the text file you downloaded. Add a layer name. In the "File format" section, select "Custom delimiters" and select "Tab". The "Geometry definition" section will be auto-populated if it finds a suitable X and Y coordinate fields. In our case they are LONGITUDE and LATITUDE. You may change it if the import selects the wrong fields. Click OK.
![Geometry definition and custom delimiters](images/kobo/image36.png)
3. Coordinate Reference System Selector will ask you to select a coordinate reference system. Since the coordinates are in latitudes and longitudes, you should select a WGS 84.
![Coordinate Reference System (WGS 84)](images/kobo/image10.png)
4. From here you will be able to view the imported data in the QGIS canvas.


### Download Forms from an Account

1. Click on the project on the KoBoToolbox home page and select “Android application” under “collect data.”
![Home screen with project saved](images/kobo/image52.png)
2. Open or install the app on the Android device. The app is called KoBo Collect. The logo should look similar to the one below.
![KoBoCollect Application](images/kobo/image2.png)
3. Go to the General Settings option on the top right of the open app. Click on “Server” and enter the URL given [online](https://kc.kobotoolbox.org) and your login information.
![KoBoCollect Home screen (menu)](images/kobo/image56.png)
4. Type in the username and password for the kobotoolbox account on the page that pops up.  Select “KoBo Toolbox” as the platform.
![KoBoToolbox](images/kobo/image6.png)
5. Return to the main menu and select “Get Blank Form” on the list.
![Get Blank Form](images/kobo/image45.png)
6. A list of all the forms from the different projects will be shown. Click Toggle All (or select the ones you wish to download), then click Get Selected.
![Get Selected](images/kobo/image9.png)
7. A pop-up should appear confirming the download is a success.
![Download Result pop up](images/kobo/image58.png)


## Collect Data with KoBoCollect on an Android Device

1. Open KoBo Collect on your device and click on “Fill Blank Form”
![Fill Blank Form](images/kobo/image4.png)
2. Select the form to which you would like to enter data and go through all the questions (swiping your finger from right to left). If you selected a “point” then use the map to determine your location. To proceed to the next question, select the arrow pointing to the dot on the top right of the screen.
![GPS demo form](images/kobo/image19.png)
If you selected a **line** as the desired answer type, then select “Start GeoTrace”. Wait for the screen to lock and add marker button to start by pressing down on where you would like to create a path. Once a pin is placed, proceed to press down on the map where you would like to connect from the previous point.
![Start GeoTrace screen](images/kobo/image33.png)
![map](images/kobo/image38.png)
If you selected an **area or polygon** as the desired answer type, then create a polygon by long-pressing the screen to create a point. Once a point is placed, continue the process to create a polygon of three or more points.
![polygon](images/kobo/image8.png)
For “Select One” answers, a drop-down screen will be available. Select one of the answers chosen to be an option.
![Is it raining? yes or no.](images/kobo/image30.png)
3. At the end click on "Save Form" and then "Exit" (making sure the form is marked as 'finalized')
![Hectacre survey](images/kobo/image5.png)

### Upload Finalized Data

* Disclaimer: Data can be collected offline and then uploaded when there is an Internet connection. However, there is an advanced option to pull data locally. A user can transfer survey data from KoBoCollect using Open Data Kit through connecting the mobile devices by USB cable to a local computer. *

1. Go to the home screen and select "Send Finalized Forms".
![Send finalized form (1)](images/kobo/image27.png)
2. A list of the collected forms appears.
![Send finalized form screen](images/kobo/image16.png)
3. Click on "Toggle all" (or select the ones you would like to send), then click "Send Selected".
![GPS demo form](images/kobo/image49.png)

### Review Data Online

1. Login to an account and select the project you wish to review
![Hectacre survey](images/kobo/image47.png)
2. Select the “Form” option at the top.
![Form](images/kobo/image40.png)
3. Under “collect data” ensure you are on “Online-Offline”  and select “Open”
![Online-Offline (multiple submission)](images/kobo/image50.png)
4.  Review the data and select “Submit”. A pop-up will appear at the top of the screen “Record queued for submission”
![Submit](images/kobo/image43.png)

### Upload Data to a computer without wifi

1. Plug in the device to a laptop or computer. A pop-up will appear on the bottom right of the desktop screen confirming successful installation.  
![Plug in confirmation](images/kobo/image11.png)
2. Another pop-up will appear, select “Open device to view files”.
![AutoPlay pop up screen](images/kobo/image24.png)
3. Click on “Internal Storage” and find the folder that says “ODK”.
![Searching in file explore](images/kobo/image14.png)
4. Copy “Instances” and place it where you want to keep the files.
![instances](images/kobo/image7.png)


## Download Data as a CSV File

1. Download data in CSV format as soon as any data has been collected. After you have sent the finalized forms, navigate to the Settings menu and select “Projects” and click on the project you wish to download as a CSV file.
![project](images/kobo/image55.png)
2.  Select the project name and scroll to the bottom of the page. Under “Data”, select “Downloads”.
![download](images/kobo/image35.png)
3. Under “Select Export Type”, click on “CSV (legacy)” in the drop-down menu. You may also change the Value and header format to XML values and headers.
![CSV](images/kobo/image12.png)
4. Click on “Advanced Export” and then “Create Export”.
![Advanced export](images/kobo/image51.png)
5. Select the links underneath “Filename”. The data should download as an excel file. Open the data in excel. Each question should have a column. With the answer in the cell below.
![excel sheet](images/kobo/image37.png)
6. This CSV file turns the data into a text file which can be labeled as an x and y points. These points can be uploaded in a table to QGIS which will plot the data for you.

### Import CSV files to QGIS

1. Open QGIS. Click on "Layers" and then "Add Delimited Text Layer".
![Add delimited Text Layer](images/kobo/image1.png)
2. In the Create a Layer from a Delimited Text File dialog, click on "Browse" to specify the path of the text file you downloaded. Add a layer name. In the "File format" section, select "Custom delimiters" and select "Tab". The "Geometry definition" section will be auto-populated if it finds a suitable X and Y coordinate fields. In our case they are LONGITUDE and LATITUDE. You may change it if the import selects the wrong fields. Click OK.
![Geometry definition and custom delimiters](images/kobo/image36.png)
3. Coordinate Reference System Selector will ask you to select a coordinate reference system. Since the coordinates are in latitudes and longitudes, you should select a WGS 84.
![Coordinate Reference System (WGS 84)](images/kobo/image10.png)
4. From here you will be able to view the imported data in the QGIS canvas.

### Download Forms from an Account

1. Click on the project on the KoBoToolbox home page and select “Android application” under “collect data.”
![Home screen with project saved](images/kobo/image52.png)
2. Open or install the app on the Android device. The app is called KoBo Collect. The logo should look similar to the one below.
![KoBoCollect Application](images/kobo/image2.png)
3. Go to the General Settings option on the top right of the open app. Click on “Server” and enter the URL given [online](https://kc.kobotoolbox.org) and your login information.
![KoBoCollect Home screen (menu)](images/kobo/image56.png)
4. Type in the username and password for the kobotoolbox account on the page that pops up.  Select “KoBo Toolbox” as the platform.
![KoBoToolbox](images/kobo/image6.png)
5. Return to the main menu and select “Get Blank Form” on the list.
![Get Blank Form](images/kobo/image45.png)
6. A list of all the forms from the different projects will be shown. Click Toggle All (or select the ones you wish to download), then click Get Selected.
![Get Selected](images/kobo/image9.png)
7. A pop-up should appear confirming the download is a success.
![Download Result pop up](images/kobo/image58.png)


-----

## View GPS Points

1. You will receive two options if your form contains any GPS questions and has submissions with coordinates
2. To view GPS points online, click the button View on Map. This will show you an online map view.
![Submisisons page](images/kobo/image17.png)
3. To modify layers on the map, select layers on the right of the screen (Google Satellite, Cloudless, Earth, etc.)
4. Another option includes downloading GPS points as KML. Click on "Download GPS Points". This will start a new export process with the latest data.
5. Previous exports will be listed by their creation date, allowing you do see snapshots of GPS coordinates at various points in time.
6. KML files can be imported in different types of software such as Google Earth or ArcMap.

### Open Data

1. Open Kobo toolbox on your laptop or tablet and choose the project you would like to open and select “Data” near the top of the page. From here you may view your data in map and tabular forms.
![Project>Table>Data](images/kobo/image54.png)

### Open Data on an Android Device

1. Open the Kobo Collect app and ensure the android application is selected under "Forms" online.
![Form: Android application. Download KoBoCollect](images/kobo/image18.png)
2. Select "Edit Saved Form".
![KoBoCollect home screen](images/kobo/image42.png)
3. Select the project you would like to open.
![Edit Saved Form](images/kobo/image13.png)
4. Review the data you have taken and save or edit the data.
![GPSPhoto](images/kobo/image41.png)

### Review Data on a Computer without Internet

1. Plug in your device to a laptop or computer. A pop-up will appear on the bottom right of the desktop screen confirming successful installation.
2. Another pop-up will appear, select “Open device to view files”
3. Click on “Internal Storage” and find the folder that says “ODK”.
4. Copy “Instances” and place it where you want to keep

With the the following R code, you can convert the local data into a csv: [https://bitbucket.org/hijmans-lab/odklocal](https://bitbucket.org/hijmans-lab/odklocal)
