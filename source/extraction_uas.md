# GeoTIFF Extraction

Contributors

[Center for Spatial Sciences](https://spatial.ucdavis.edu/)

[University of California, Davis](https://www.ucdavis.edu)

-----

**Introduction**

Collecting research quality data with small Unmanned Ariel Systems (UAS) requires users to be aware of the many factors that can affect their platform and its data outputs. This operating procedure outlines the methods used by the UC Davis Grain Cropping Systems lab in extracting data from UAS imagery. This procedure could be used for other forms of multispectral imagery as well.
*These instructions are based on QGIS 2.18.22 Las Palmas.*

-----

**Setup**
1. Create a new project: "Project" > "New"
1. Add a clearly captured GeoTIFF of our field: "Add Raster layer" (see figure 1)
![Image](images/extraction_uas/image1.png)
*Figure 1.*
1. Navigate to and choose your image file.

----

**Shapefile Creation**
1. Create a new layer: "Layer" > "Create Layer" > "New Shapefile Layer..."
1. Set the properties of your shapefile:
    1. Select “Point"
    1. Set the coordinate system to match your GeoTIFF (see Figures 2 & 3)
    ![Image](images/extraction_uas/image2.png)
    *Figure 2. In the Layers panel, right click your GeoTIFF layer and add choose "Properties" to see the CRS.*
    ![Image](images/extraction_uas/image3.png)
    *Figure 3. Choose the CRS that matches your GeoTIFF layer (WGS84/UTM zone 10N for most Northern California M100 flights)*
1. Click "OK"
1. Name and save your shapefile when prompted.
1.	Edit the shapefile:
  1.	Make sure the shapefile you created is highlighted in the layers panel.
  1.	Toggle editing on by clicking the yellow pencil in the top left (see Figure 4)
  ![Image](images/extraction_uas/image4.png)
  *Figure 4. Add Feature to a shapefile*
  1. Add a feature by clicking the "Add feature" button (see Figure 5)
  ![Image](images/extraction_uas/image5.png)
  *Figure 5.*
  1. Using your curser click a point at the center of each plot you wish to extract data from.
          1. Make sure to create these points in order of plot number for easy data manipulation later.
      1. Set the ID of each point as a consecutive integer.
      1. Repeat for as many features as you desire for that image
1. End your edit session by clicking the "Toggle Editing" button again.
1. Save your edits when prompted.
1. For rectangular buffers, use the rectBuffers.py custom script.  
  1. Save “rectBuffers.py” in your “\.qgis2\processing\scripts" folder
    1. Copy located in \Box Sync\Grain Cropping Systems Lab\Drones
  1. In the processing toolbox open “Rectangular Buffers” under “Scripts” > “User scripts” (see figure 6).
  ![Image](images/extraction_uas/image6.png)
  *Figure 6. Run the custom script from the Processing Toolbox*
  1. Enter the height and width of your desired rectangle in meters (given that the projection is in UTM).
  ![Image](images/extraction_uas/image7.png)
  *Figure 7. Enter the parameters for the extraction shapes in the Rectangular Buffers input*
1. Add plot level metadata that can differentiate the plots by opening the Attribute table while editing is on.
  1. Ctrl + W to create a new field
  1. Name the new field appropriately and populate with metadata
1. Save the rectangular_buffers shapefile by right clicking the layer and choosing "Save As..."
  1. For small grain variety trial images save shapefile as Location_Year. i.e. Davis_2018.shp  

----

**Georeferencing**

  **With ground control points in Pix4D:**

		<https://support.pix4d.com/hc/en-us/articles/202558699-UsingGCPs#gsc.tab=0>

  **With tie points in QGIS:**  
1. Open QGIS
1. Project > New
1. Add a raster layer of your base image.
![Image](images/extraction_uas/image8.png)
![Image](images/extraction_uas/image9.png)
1. Open the Georeferencer.
![Image](images/extraction_uas/image10.png)
1. Choose the raster you want to Georeference by opening the raster and navigating to the image you want to georeference
![Image](images/extraction_uas/image11.png)
![Image](images/extraction_uas/image12.png)
1. Zoom and pan to a place in the image that will be there for the entirety of the season or the time period when you are planning to fly. Using the center of a plot works well.
![Image](images/extraction_uas/image13.png)
1. Choose the yellow "Add Point" button at the top of the interface
![Image](images/extraction_uas/image14.png)
1. Click on the tie point you have chosen. Then choose "From Map Canvas"
![Image](images/extraction_uas/image15.png)
1. Pan, zoom and click on the same point on the base image you loaded onto the map canvas.
1. Then click “OK” to save the point.
![Image](images/extraction_uas/image16.png)
1. Do this for 5 – 10 points around your image.
1. When you are satisfied with the number of ground control points, choose “ Start Georeferncing” (the green play button)
![Image](images/extraction_uas/image17.png)
1. If asked for the transformation information you can stick with the default. Check Load in QGIS when done and set the name of your output raster.
![Image](images/extraction_uas/image18.png)
1. Press the green "Start Georeferencing" button again. The georeferenced image will be saved and loaded onto the canvas.
1. You can check that it aligns with your shapefile by loading the shapefile onto it.

----

**Adjust Shapefile**

1. To adjust the shapefile, save the file with a new name.
  1.	For small grain variety trial images save shapefile as Location_Date. i.e. Davis_4l3l2018.shp (those are the letter “L” between month, day, etc.)
1.	Toggle editing on for the new shapefile.
1.	Select all (or the subset that you wish to adjust) by choosing the “Select Features by area of single click” (see Figure 8) and drawing a rectangle around the features you want to adjust.
![Image](images/extraction_uas/image19.png) *Figure 8. Select features tool*
1. Move or rotate using the appropriate tools (see Figure 9 and 10).
![Image](images/extraction_uas/image20.png) *Figure 9. Move Feature(s)*
![Image](images/extraction_uas/image21.png) *Figure 10. Rotate Feature(s)*
1. Toggle editing off to save.

----

**Value extraction**

1. Check the installation of the "Zonal statistics plugin"
  1. “Plugins” > “Manage and Install Plugins”
  1. Type Zonal Statistics in the search bar
  1. The “Zonal Statistics Plugin” should appear checked as in Figure 5.
![Image](images/extraction_uas/image22.png) *Figure 11. Zonal statistics plugin correct installation*
1. Extract the image values using the "Zonal statistics plugin"
  1. Go to "Raster" > "Zonal statistics" > "Zonal statistics"
![Image](images/extraction_uas/image23.png) *Figure 12. Navigating to the Zonal statistics plugin*
  1. Choose the polygon layer and the band you wish to extract.
  1. Check whichever boxes you want data for, e.g. "mean", "median", "mode", "standard deviation", "minimum", and "maximum".
    1. For small grain variety trial images set the prefix to match the band i.e. "blue_", "green_", "nir_", "edge_"
      1. Shapefile column names have a limited number of characters so follow these exactly.
    1. Check "mean", "median", "standard deviation", "minimum", and "maximum"
  1. Click "OK" to run the algorithm
      1. For small grain variety trial images repeat for each band (should be a blue, green, nir, and red edge band)
1. Look at the values you extracted by right clicking on your shapefile layer and navigate to "Open Attribute Table" (see Figures 13 & 14)
![Image](images/extraction_uas/image24.png) *Figure 13. Navigate to Attribute Table*
![Image](images/extraction_uas/image25.png) *Figure 14. Example Attribute Table*
1. Save the shapefile and close QGIS

----

**Value Analysis**

1. To open table in Excel:
  1. Make sure to QGIS is closed.
  1. Open Microsoft Excel
  1. Choose “Open Other Workbooks” or “File” > “Browse”
  1. Navigate to the file where you saved your shapefile
  1. Make sure “All files” is selected in drop-down list at bottom right, NOT “All excel files”
  1. Select the .dbf file you just saved
1. To open the attribute table in R use the following code below. Adjust the file path accordingly.

    `filepath <- "C:/Users/LundyAdmin/Documents"
library(foreign)
read.dbf(file.path(filepath, "Esparto2017_18.dbf"))`
