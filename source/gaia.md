# Gaia GPS: Hiking, Hunting Maps

Sierra Mabanta, Emily Hurry, Arielle Rose & Alex Mandel

[Geospatial and Farming Systems Research Consortium University of California, Davis](https://gfc.ucdavis.edu)

This guide helps you to Create:
* Waypoints
* Polygons
* Routes

What you'll need:
* An iOS device
* [Gaia App for iOS](https://itunes.apple.com/us/app/gaia-gps-hiking-hunting-maps/id1201979492?mt=8)

*Also available for Android. However, instructions are for iOS only. If desired, link for Android is [here](https://play.google.com/store/apps/details?id=com.trailbehind.android.gaiagps.pro).*

---------------

## Create and Add Waypoints

Please note: Some features of this application may be unavailable. Instructions below are intended and written for the free-version of this application. Gaia GPS for free is limited to the default map source (Gaia Topo), and cannot download maps for offline use.

Visit the website, learn more, and start a free account at:  https://www.gaiagps.com

1. Open the app and locate the plus sign symbol at the top of the page and tap it. A drop-down menu will appear, select “Add Waypoint”.

![Drop-down menu](images/gaia/image2.png)

2. You will also notice a pin on the map of your current location. Zoom out of the screen and drag the pin wherever you would like on map. A box will appear with a latitude and longitude on the screen. If you know the latitude and longitude of the desired location, you may fill this in and the point will relocate to that specified destination.

![Map with waypoint](images/gaia/image4.png)

3. Select “Save” and give the location a title and customize the appearance. You will notice a tab on the bottom called “Suggested titles”, this will label the location by its address.

![Name and Save drop-down menu](images/gaia/image5.png)

4. You can customize the symbol appearance by selecting the icon.

![Icon options](images/gaia/image3.png)

## Create a Polygon

1. Drag the map over the area you would like to create a polygon and click on the plus sign symbol on the top of the screen and select “Create area”.

![Drop-down menu of "Add Waypoint"](images/gaia/image9.png)

2. A polygon will appear. Drag the nodes (blue circles) to adjust to the area you would like to cover.

![Polygon overlay on map with 4 nodes.](images/gaia/image12.png)

3. Add new nodes to the polygon by tapping and holding the line that connects the two points to one another.

![Abstract polygon with 13 nodes](images/gaia/image8.png)

4. Select “Save” on the top right of the screen when you are finished. Name the area and save again.

![Pop up text area to enter text of area name.](images/gaia/image6.png)

## Create a Route

1. Return to the main menu and select the plus sign symbol, select “Create Route” and notice the blue point that appears on the center of the screen.

![Point on map.](images/gaia/image7.png)

2. Move this point to the starting point of the route. Long-press on the screen to create another point. Continue this process until the route is completed with desired detail.

![multiple node connected by red line.](images/gaia/image11.png)

Save the route with a title.

![Search screen with saved polygon, waypoint, and route.](images/gaia/image1.png)
