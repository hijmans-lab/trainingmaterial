# Maps.Me for Geographic Data Collection

Sierra Mabanta, Arielle Rose, & Alex Mandel

[Geospatial and Farming Systems Research Consortium University of California, Davis](https://gfc.ucdavis.edu)

----------
The following guide is created for offline use.

  1. This guide helps you to:
* Cache maps
* Create and save bookmarks
* Using Maps
* Export and save maps

What you’ll need:

* A phone or tablet and a computer
* Maps.Me App
    * [iOS](https://itunes.apple.com/us/app/maps-me-offline-map-nav/id510623322?mt=8)
    * [Android](https://play.google.com/store/apps/details?id=com.mapswithme.maps.pro)

*Application is compatible with: tablets, ipads, iOS phones, and Android phones.*

----

Visit the website to learn more and start a free account at: https://maps.me/download/

## Cache Maps *for Offline Use*

1. Ensure location settings are on by going to your devices setting, select "Security and Location", then under the privacy section toggle "Location" on and choose a desired accuracy.

**Pro Tip**: Higher accuracy of device location will use more battery as a trade off. If battery life is not an issue on your particular device, the higher accuracy will work most effectively.

Open the Maps.Me app. Click on the menu indicated as bars on the bottom right of the page and select “download maps”.

![menu bars](images/mapsme/image23.png)

![Download Maps](images/mapsme/image11.png)

2. Type in the map you wish to download and select the location download button to the left. A pop up will appear at the bottom of the screen. Select “Download map”.

![Search bar](images/mapsme/image5.png)

![Download Map bar](images/mapsme/image6.png)

3. A check mark will appear next to the selected map once you have successfully downloaded the map. Repeat this step for each map you wish to download.

## Create and Save Bookmarks

1. Type the location you wish to bookmark by clicking on the magnifying glass on the bottom left of the screen. Select the location name and click on the star on the pop up to bookmark the location.

![search results](images/mapsme/image20.png)

![bookmark](images/mapsme/image3.png)

2. Scroll up from the bottom to view the coordinates under the "Edit Bookmark" section. You can copy a preferred format of the coordinates by clicking on them.

![""](images/mapsme/image8.png)

![Create a copy](images/mapsme/image18.png)

3. You can bookmark an unlimited amount of searched locations. Use the settings to the right of your set to change the name and to hide them from view (tap the eye icon).

!["Research Locations, My Places"](images/mapsme/image12.png)

!["My Places"](images/mapsme/image14.png)

4. To edit these locations, long-press on the location. A pop-up will appear at the bottom of the page. Select “Edit”, here you can change the bookmark color, set, and description of the location.

![Long-press menu](images/mapsme/image1.png)

![Bookmark color options](images/mapsme/image21.png)

5. To export these bookmarks, tap on the location and select “share”. To export an entire set, long-press the set on the list and tap “Share”.

![Long-press menu](images/mapsme/image1.png)

![Share option](images/mapsme/image2.png)

6. Select a method of sharing bookmarks.

![Sharing options](images/mapsme/image7.png)

## Using Maps *for Offline Use*

1. Click on the menu icon and select “Downloaded Maps”.

![downloaded maps](images/mapsme/image22.png)

2. A list of the downloaded maps will appear. You can also search for it in the search bar above. Click on the map to view.

![List of created maps](images/mapsme/image15.png)

## Export and save maps for online use with an Android  Device

1. Open the app and type the location of the map you chose to export.

![Search results](images/mapsme/image4.png)

2. Select "Download". Once the download is complete, you can now access it offline.

![Download in progress](images/mapsme/image16.png)

3. Maps can be saved on your SD card (if applicable). Select "Share".

![Share](images/mapsme/image19.png)

![Share options](images/mapsme/image17.png)

4.  Choose "More" and then "Settings".

![Settings menu](images/mapsme/image13.png)

5. Choose the units under "Maps Storage". Select the storage device you wish to choose and save maps to the SD card.

![""](images/mapsme/image10.png)
