sUAS Flight Operations Manual for Data Collection
=================================================

Emily Hurry, Sierra Mabanta, Arielle Rose, Anna Gard, Darcy Bostic, Alex Mandel


.. toctree::
   :maxdepth: 1
   :caption: Flight Ops:

   Condition Checks <conditioncheck>
   Ground Control Points <gcp>
   GPS Guide <gps>
   Flight Planning <flightplanning>
   Flight Operations <tasks>
   Data Processing with Pix4D <dataprocess>
   Data Processing with Photoscan <../Agisoft>
   Stitching and Stacking Imagery <stitchstack>
   Equipment Checklist <uavlist>
   Charging Aircraft Batteries <battery_charging>
   Managing Data <upload>
