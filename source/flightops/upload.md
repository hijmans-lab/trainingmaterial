#  Saving and Uploading Data
###  Flight Logs
1. Take a picture of your flight log. Fill out post flight report and submit image of flight log.

###  Saving Files
1. Please save and upload your folders into the following structure

  * Location
    * Date (YYYYMMDD)
      * Camera
          * Results
          * Raw
          * GCP (if applicable)
  * Ex) Davis
    * 20170214
      * Sequoia
        * Flight
      * X3
        * Flight

File names should be Location_YYYYMMDD_Camera_Flight
