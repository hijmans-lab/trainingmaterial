
#  Ground Control Points
*Ground Control Points (GCPs) are necessary for accurate data because they tie your imagery to a known, identifiable, measured coordinate.*

![Example Ground Control Points](/images/flightops/image41.jpg)
1. If necessary put out GCPs. These should be easily identifiable from the air and if applicable, differentiable.
  * 9 is the recommended minimum
2. Turn on GPS and then turn on Blu Bluetooth. It should connect automatically. See [GPS Guide](gps.html) to check for pairing and connecting
3. Check connection using the GNSS App and view detailed status report
4. Place GPS on top of the center of the Ground Control Point
5. Open KoBoColllect app. Click the three dots in the upper right side to access general settings. Check the URL. For more information see the [KoBoColllect instructions](../kobo.html)
6. From main screen click *Get Blank Form* and select GPSPhoto
7. Then click *Fill Blank Form* and select GPSPhoto
8. Click Record Location.Wait for Phone to collect location. Swipe right to get to the next screen.
9. Take a photo of the GCP - Name the photo as the Ground Control Point Identifier. Fill in all relevant fields of the form.
10. Repeat for every GCP in your study area. You are creating a form that should have the location and a photograph of every ground control point.
11. When phone is online, click Send Finalized Form to upload your data.
12. Download the data as a csv file from KoBo to get a list of coordinates and names.
