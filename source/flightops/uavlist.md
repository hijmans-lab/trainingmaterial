
# UAV Checklist –  Backpack

*  Laptop
*  GPS
*  Blu Phone
*  Tablet or iPad
*  HoldPeak Pro Anemometer
*  Chargers for
  *  GPS
  *  Blu Phone
  *  Tablet
  *  Laptop
*  Rain Cover
*  Camera Cleaning Kit
  *  Magic Fiber
  *  Mini Magic Fiber x2
  *  Air pump
  *  Brush
  *  Retractable Brush
  *  Cleaner Spray
  *  Lens Tissue
*  Vinyl Electrical Tape
*  Safety Glasses
*  Pens/Pencil
*  UAS Pilot Log
*  Backup Hard Drive and Cable
*  Memory Card and Adaptor
*  Landing Pad Pegs



# UAV Checklist - Duffel

*  Tripod
*  Tripod Table/Laptop Shade
*  Landing Pad
*  Propeller Guards
*  Loctite (Optional)
*  220 V Electric Duster (Optional)
*  Spare screws and dampers (Optional)


# UAV Checklist –Aircraft

*  Drone
*  Camera x 2
*  Propeller
*  Spares
*  Power Pack
*  Batteries
*  Sunshine Sensor
*  Screw Drivers
*  Spare airplane plate
*  Tablet Sun Shade
*  Calibration plate
*  Manual
*  Controller + Wire
*  Safety Strap
