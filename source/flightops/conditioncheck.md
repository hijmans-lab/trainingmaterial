
# Pre-Flight Conditions Check
The following is a list of conditions to be checked to ensure safe flight operations
**Best done day before flight**
1. Check all required offline data is downloaded correctly on devices. (cached maps, flight plans, Sequoia shortcut ,etc) [Flight Planning Instructions](flightplanning.html) can be found here.
2. Pack and check Aircraft and Backpack, reference [checklist](uavlist.html).
3. Check Batteries and charge batteries to full charge.
4. Calculate Solar Noon using one of the following sites/tools
    * [SunCalc](http://suncalc.net/#/38.563,-121.76,12/2018.02.20/13:32)
    * [NOAA Solar Calculator](https://www.esrl.noaa.gov/gmd/grad/solcalc/)
    * Use R package [SunCalc](https://cran.r-project.org/web/packages/suncalc/README.html)

5. Check weather conditions and necessary weather maps before flight
    * Rain - Do not fly if Raining, Foggy or Misty
    * Temperature - Do not fly if below -10°C or above 35°C
     * Check [this](https://www.wunderground.com/) website for detailed temperature
     * Compare to heat index chart. Do not fly above 35°C (or equivalent heat index)
     ![HEAT INDEX](/images/flightops/image29.png)
    * Wind Speed & Direction (where the wind comes from in compass bearing or Cardinal directions)
      * Do not fly if Wind Speed is over 11 m/s at ground level. Recommended 6 m/s maximum at ground level for data quality control.
    * Okta Scale for clouds- Data Collection is best with 0 or 8

    ![OKTA SCALE](/images/flightops/image46.png)

### Launch Site check
** Before Take Off**
#### Inspection
1. Check for potential hazards at test site. Ensure your visual observer can scan for any other aircrafts or obstacles.
2. Create flight log entry. Note and record weather in field before flight (Temperature, wind speed, wind direction, cloudiness (Okta scale))
3. Locate a level flat surface with at least 3 meters wide to launch from. Clear all stick or tall grass that may be hit by propellers. Set up Heliopad
4. Begin safety checklist. Inspect Aircraft, Equipment, and Remote Control for any cracks, loose wires, or damage
5. Attach Cameras
    * Check for memory cards
    * Clean lens
6. Batteries
    * Check the charge by pushing the battery button once.
    * Record your battery numbers in log
    * Insert batteries - Partially slide in and then push quickly in until battery clicks
     * If you are only flying with one battery please use the main power source (the back battery ((with the USAID sticker))), and disconnect power to the other battery bay.
7. Attach Propellers by twisting them down and in
  *  Ensure dots match dots and that they are secure and tight. Twist in the direction indicated on propellers
8. Update Pilot Log, Review for the following
   * Pre-flight checklist
   * Memory Cards
   * Note Battery ID, Cameras, AGL of flight plan, %overlap, planned speed if known (you can record actual speed later).
![Set Up](/images/flightops/IMG_8404.jpg)

#### Power
*Note: to turn on all DJI equipment press once, release, press again and hold down power button for 2 seconds.*
1. Turn on the Device and Controller
    * Blu Phone / iPad - Turn device on, turn controller on, plug in
    * Samsung Tablet - Plug into controller, Turn on controller, then start/restart tablet. App should open by default.
2. Power on aircrafts
3. Check the DJI Go app to ensure drone is working properly (Blu Phone or iPad only, may require device reboot to change apps)
    *May have difficulty connecting, power cycle and try again*
    * Note battery charge
    * Note updates available

#### Sequoia
1. Check camera and sensor securely attached and wired
2. Connect to wifi with either another device, or if only one is available, the device in controller.
     * Connect to network  Seqouia_####
     * Go to 192.168.47 in internet browser or use shortcut if made
     * Set camera to take pictures at GPS intervals. Check conversions for measures
![GPS Capture Mode](/images/flightops/image43.png)
3. Take Calibration photos
     * Hold drone directly above plate and step aside to remove your shadow
     * Take photo of calibration plate by triggering remote calibration from wifi interface
4. Launch (Start Capture) by clicking the green button next to the RGB one at the bottom of the page
  * Stop capture on the sequoia by connecting to its wifi and clicking the same button that started capture.
![Start capture](/images/flightops/image22.png)
##### Conversions for Parrot Sequoia
![Height and Distance between shots](/images/flightops/image47.png)
![Height and Time between shots](/images/flightops/image23.png)



#### Flight Plan
1. Open app and check base map has loaded. Check all settings
2. Upload flight plan
3. Ensure flight crew is ready and that it is safe for take off

#### *Take - Off!!*
![Take Off Image](/images/flightops/IMG_8413.png)
### Post Flight check
**After Landing**
1. If conditions have changed during flight - take new calibration photos
2. Power off and remove batteries
   * Push button once, release, push and hold buttons for 2 seconds, lights will count down.
   * Push groves on side and pull
3. Power off controller
4. Inspect for damage or wear
5. Update Pilots Log
6. Remove and store propellers and cameras if flight operations are finished.
  * Push down and turn in the direction indicated by the unlock diagram on the blade.
7. Optional - Download photos to laptop to verify data collection
  * Remove MicroSD card for RGB camera
  * Plug usb cable from laptop for Sequoia

### Reference Images
1. Aircraft
![Aircraft](/images/flightops/AirCraft.png)
2. Flight Logs
![Aircraft](/images/flightops/flightlg.jpg)
