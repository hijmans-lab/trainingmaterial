# How to Charge a Lipo Battery

Contributor: Anna Gard

[Center for Spatial Sciences](https://spatial.ucdavis.edu/)

[University of California, Davis](https://www.ucdavis.edu)

-----
#### Introduction

The following instructions will explain how to safely charge a 8000 15C mAh battery (Figure 1) using a Thunder AC6 Dual Power charger (Figure 2).  NEVER leave a charging battery unattended. To avoid serious damage in the event of a fire (1) charging must always be suLpervised and (2) battery must be in lip guard bag while charging.



![Image](/images/lipo/battery.png)
*(Figure 1)*

![Image](/images/lipo/charger.png)
*(Figure 2)*


_____

#### instructions

1. **Before** plugging the charger to the power source, connect the battery to the charger *(Figure 3)*.
  - Connect the yellow ends together.
  - Connect the white tip from the battery into the 4S slot on charger adaptor.
  ![Image](/images/lipo/connected.png)
1. Place the battery in the Lip Guard bag, close it as best you can.
![Image](/images/lipo/case.png)
1. Plug power chord into the wall outlet. The screen should turn on with the settings from the previous use.
1. If the LED screen matches ![Image](/images/lipo/ledscreen.png), press and hold the green "Start Enter" ![Icon](/images/lipo/start.png) button until it beeps, charging will begin.
1. If your screen does not match, you will need to adjust the settings.
1. Should you need to stop charging for any reason, press and hold the "Batt Type Stop" button ![Icon](/images/lipo/stop.png).
1. When the battery is finished charging, the charger will beep a couple of times. The beeping is very extreme, and may scare you the first time you hear it.
1. Unplug the power chord from the wall. The LED screen should go dark, indicating the battery charger is off.
1. Disconnect the battery from the charger, and place it back in the Lipo Guard for safe storing. If you want to confirm a full charge, you can easily do so (instructions below).

----

#### Adjusting the Settings

1. Press the "Start Enter"![Icon](/images/lipo/start.png) button once. The bottom left number should start blinking. Use the arrows ![Icon](/images/lipo/arrows.png) to change the setting (instructions below).
1. When you are satisfied, press the "Start Enter" button to move on to the next setting.
1. When the settings match, double click the "Start Enter" button to exit edit mode.
1. Now you are ready to begin charging the battery.

-----


#### Checking the Battery Charge

1. This Lipo Battery Voltage Checker can be used for 1-8 cell lipo batteries. It reads individual cell voltage and total pack voltage.
![Image](/images/lipo/chargereader.png)
1. Connect the device to the white connector on battery. The device will beep then start reading the individual cell charges, followed by the total pack voltage. ![Image](/images/lipo/chargereaderconnected.png)
