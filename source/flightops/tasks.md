# Team Roles
## **Pilot in Command**
*This is the person holding the FAA License and likely also the pilot*
### Primary Tasks
  * Check all Pre-flight conditions and safety regulations
  * Maintain sight of drones at all times during operation
  * Be prepared to manually land/adjust drone at all times
  * Oversee operations and ensure safe conditions at all times

## **Observer A**
*This person is the pilots right hand man and ensures safety of pilot*
### Primary Tasks
  * Assisting pilot with moving during flight
  * Reading off Flight Planner Screen during flight
  * Observing skyline in direction pilot is facing
  * Taking Calibration Photos

##  **Observer B**
*This person is in charge of the flight log and record keeping*
###  Primary Tasks
  * Initiating and Recording pre-flight safety checks
  * Filling out Flight Log
  * Connecting to Sequoia camera with tablet/phone
  * Launching radiometric calibration before flight
  * Launching gps lapse for flight
  * Recording start/end times for flight
  * Observing skyline behind pilot
