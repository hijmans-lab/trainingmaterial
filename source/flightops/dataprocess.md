#  Post Processing with Pix4d and Ground Control Points

##Table of Content
* Project Set Up
* Begin Processing
* Calibration Images
* Ground Control Points
* Finish Processing and Upload
* Misc Note

##Project Set Up
1. Upload Data to Night Fury - [Full instruction here](upload.html)
  * This PC > Users > Public > Public Pictures > Flights > Location_Date >
  * Rename data appropriately (West instead of 001)

![Example of Data Upload and Structure](/images/flightops/image35.png)
2. Open Pix4Dmapper (application on computer)
  * Login (given by lab)
3. Click new Project

![Example of New Project in Pix4D](/images/flightops/image33.png)
4. Name the Project Location_YYYYMMDD_Type_Field_Camera and create
  * Example: RR_20180523_Multi_East_RedE
5. Select all Imagery from corresponding Public Flights Folder
  * If the program highlights certain images and says there is an error, click the highlighted image and select “remove selected”. It may take the program a few moments to read EXIF data and create a camera rig.

![Adding Images to Project](/images/flightops/image6.png)

6. Review Image Properties and ensure all checks are green. Change Geolocation Accuracy to “Standard” and click Next.

![Image Properties](/images/flightops/image19.png)

7. Review Output Coordinate System. Leave on auto detected if Nzone 10. Click “Next”

8. **Select** the correct Processing Options Template (guide below) by highlighting and click “Finish”.
- Ensure “start processing now” is **NOT** checked (located at the bottom right of corner)
  * Standard 3d Maps **for RGB** for stock camera
  * Advanced Ag RGB **for Sequoia RGB**
  * Personal Merge Ag **Multispectral for Sequoia /RedEdge Multi**

Click “Finish”

9. Click Processing options and ensure settings for output for correct
  * This is what personal Ag Multispectral should look like for the tabs in Step "3. DSM, Orthomosaic and Index"
![Ag Multi Merged Settings](/images/flightops/image28.png)
![Ag Multi Merged Settings](/images/flightops/image2.png)

    * 3. DSM, Orthomosaic and Index > Index Calculator > Reflectance Map
    * 3. DSM, Orthomosaic and Index  > DSM and Orthomosaic > Raster DSM
    * 3. DSM, Orthomosaic and Index  > DSM and Orthomosaic > Orthomosaic

  * Under Resources and Notifications slide down the RAM and CPU 1 tick and click "Okay"
![Reduce Resources for Processing](/images/flightops/image7.png)

## Begin Processing

10. Run Step 1 - 1. Initial Processing
  * Specify which step you would like to run by unchecking the steps you do not want to run. This can be done through the processing options or in the main page under processing

![processing](/images/flightops/image37.png)

Click “Start”

11. Once step 1 is done, click “Processing Options” gear icon on the bottom left.

## Calibration Images

If doing multispectral processing, you will need to add calibration images. **If you are doing RGB you can skip step 12.**

12. Add calibration photos by going to the Radiometric Processing and Calibration section in the Index Calculator tab under 3. DSM, Orthomosaic and Index.
  * Correction type should be Camera and Sun Irradiance
![Calibration Options](/images/flightops/image12.png)
* Select Calibrate and then Browse to select the correct calibration image for the band.
    * Sequoia - There will be multiple to choose from and some may cause errors so try the one with most balanced looking exposure
    * You should be able to choose pre or post calibration - use your best judgement
    * Draw a box around the panel. Fill in the reflectance factor based on the numbers provided in the Calibration Panel section of the instructions manual. Divide by 100 so value is less than one. This may be filled in automatically.
    * Repeat for all 4 (sequoia) or 5 (RE) bands. Click okay
![Identifying Calibration Panel](/images/flightops/image20.png)

## Ground Control Points

All projects will need ground control points added. This allows us to compare flights over time.

13. Open GCP/MTP Manager under project drop down menu or by clicking the target icon
  * Change the coordinate system to 4326 by clicking “Edit” in the top right hand corner.
  * Click Import GCP and select your csv file. (csv files should be in name, lat, long, alt format. See downloading data from KOBO file for more information)
  ![Importing GCPs](/images/flightops/image16.png)
14.  If it says GCPs already exist do you want to replace, click “Yes to all”. You should now see the GCPs listed in the table area. Click rayCloud Editor

Step 15 may or may not help with displaying the images in the next section but it doesn’t hurt to turn off thumbnails

15. Under “Cameras” drop down in the “Display Properties” menu, uncheck thumbnails.

  ![Removing Thumbnails](/images/flightops/image34.png)

**Pro Tip:** Make sure you select the correct GCP as some images will contain more than one of your targets. Be as zoomed in and centered as possible when marking the GCP, this will allow for more accuracy. The image below is a before and after.


![GCP Identifying](/images/flightops/image4.png)

16. Under “Layers”, click the drop down menu under “Tie Points”. Click the drop down for GCPs.

17. All of your GCPs are under this tab. Notice there is a number associated with each GCP, this is the amount of images the computer recognised that may have the GCP in it.
* When you click on one of the ground control points the associated images will display in the bottom right window. This is where you correct the location of the “blue target” by clicking on the true GCP adding a “yellow target”.

18. Once you have selected at least 2 GCPs, a green “x” will appear in the remaining photos indicating where it thinks the new location is.
* If you are satisfied with the location of the green “x” you can select “Automatic Marking” and then “Apply” to set the new location.

![Identifying GCP in Ray Cloud](/images/flightops/image21.png)

**Pro Tip:** When processing Multispectral Data everything is in greyscale. Green band shows the most familiar levels of contrasts and therefore will be easiest to view the GCPs.

![Multi GCP](/images/flightops/image14.png)

19. Continue for all your GCPs, or at least 6-9 points.

Ideally, you will have truthed many GCPs throughout the map that evenly spread out. This will allow for a more accurate coordinating of all the images.

## Finish Processing and Upload

20. Under “Process” in the top bar, select “Reoptimize”

21. Once done with step 24, Run Step 2 - Point Cloud Mesh and Step 3 - DSM, Orthomosaic and Index

Congratulation! If everything went smoothly you have now completed the processing in Pix4D.

22. Copy results folder into Google Drive, under the correct date_location folder.
  * Also drag a copy of the raw data into the project folder if not already done.
  * Copy just the final result map into the results folder for that project

  ## Misc Note

Complete Process
* Upload Data to Night Fury
* Rename Folders
* Upload to Google Drive
* Create Project in Pix4D
* Run Step 1
* Calibration Images for Multi
* Ground Control Points
* Reoptimize Data
* Run Step 2 and 3
* Merge Multi Reflectance Tiles
* Upload Folder to Flight Data
* Upload Results to Results Folders
