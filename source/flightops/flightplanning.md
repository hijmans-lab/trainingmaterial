# Instructions for using the Precision Flight planning software
We use Precision Flight because we like having the ability to pause and resume flights, fine control of the overlap, offline caching of basemaps, cross platform availability, and the ability to import/export plans. Other apps we recommend include:

 * Drone Deploy
 * DJI GS Pro.
The planning is roughly the same, although each app has different pros and cons depending on your flight goal.  

## Office Planning

1. Download the app
  ![App Icon](/images\flightops\app.png)
2. Open PrecisionFlight app and click plan a flight. Choose grid format.
3. Name flight plan and set flight altitude. Good Names include location and either altitude or flight date. Altitude is normally between 40 and 80 meters.   
4. Pan and zoom to test location on the interactive map. The search icon in the upper right side lets you enter location.
    * The GPS icon will recenter back on your devices location.
5. Click draw and adjust box as needed. The lines direction can be rotated using the rotate tool at the bottom of the screen. We recommend a north-south orientation so the sun angle is consistent.
6. Open advanced setting and go to Mission settings. Set overlap, side and front, to about 80% and flight speed around 8m/s. These are settings for the X3 which are compatible with the multispectral camera, but always [check the conversions](conditioncheck.html) for your flight.

  ![App Icon](/images/flightops/IMG_0078.png)

6. Save flight
7. On the main menu select your flight. Under the options icon select download maps for offline use. Test maps have been cached by reopening the app in airplane mode.
![App Icon](/images/flightops/homepg.png)

## Sending and Uploading Flight Plans

1. If you select More you can have the options to duplicate, delete, or send your flight plan. Sending your flight plan will send your flight as a geojson file which can be saved to your devices cloud or drive.
2. You can bring this geojson into qgis or edit with notepad++ to adjust coordinates. You should be able to send the edited file back and redownload it.
This is also useful if you are switching devices.
![App Icon](/images/flightops/IMG_0080.png)
