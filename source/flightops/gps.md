
# Trimble R1 GNSS Receiver Set-Up and User Guide

## Downloading Applications and Connecting to Android Device

1. Download both the [GNSS Status](https://play.google.com/store/apps/details?id=com.trimble.mcs.gnssstatus) and [GNSS Direct](https://play.google.com/store/apps/details?id=com.trimble.gnssdirect) applications from the Google Play Store.

2. Enable mock locations on the Android Device under Settings > Developer options > Debugging > Select mock location app. Choose GNSS Status as the mock location app.

  ![Mock Locations](../images/flightops/mock.png)

3. Turn on the Trimble R1 GNSS Receiver by holding the Power Button until both the LED lights turns a solid green. The Bluetooth LED light should then start flashing an alternating blue and yellow.

4.  Open GNSS Status application on the Android Device.

  ![GNSS Status Icon](../images/flightops/GNSS.png)

* If this is your first time pairing the Trimble R1 Device to your phone, please do the following:

    Put Trimble R1 GNSS Receiver into pairing mode by holding the power button for 5 seconds until the Bluetooth LED light begins to blink a rapid blue (ignore the battery LED light).

5. Under the “Source” tab, select "Bluetooth" as the "Position Source". Select the Bluetooth connection with a name similar to "GNSS:51234" (The five digit number will correspond to the last five digits of your Trimble R1 GNSS Receiver's serial number located on the back of the device). The app should then navigate you back to the "GNSS Status" tab.

    * If the device is not pairing, please refresh the Bluetooth connection on the Android Device.


  ![Source Tab](../images/flightops/image44.png)

6. Under the "Corrections" tab, set the "Primary" to "RTX (via Satellite)", the "Satellite Selection" to "Auto", and the "Secondary" to "SBAS."

  ![Corrections Tab](../images/flightops/correct.png)

7. Once the receiver connects to satellites, the estimated accuracy should appear. Wait about five minutes until it is about half a meter (50cm).

  ![Home Tab](../images/flightops/image5.png)

8. To turn the Trimble R1 GNSS Receiver off, hold the power button until the Bluetooth LED light or both LED lights go from red to off.

Reference from the Trimble R1 GNSS Receiver Quick Start Guide:

  ![Trimble R1 GNSS Receiver Quick Start Guide Page 2](../images/flightops/guide_pg2.jpg)

  ![Trimble R1 GNSS Receiver Quick Start Guide Page 3](../images/flightops/guide_pg3.jpg)
