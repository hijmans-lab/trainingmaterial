.. GFC Guides documentation master file, created by
   sphinx-quickstart on Wed Jul 18 10:39:08 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Guides to Geospatial Tools
======================================

Field Navigation & Data Collection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `Kobo Toolbox <kobo.html>`_ Field survey tool with support for spatial data recording.
* `Maps.me <mapsme.html>`_ Portable map and navigation software. For use when OpenStreetMap is the best base data source, or when you won't have network access.
* `Gaia <gaia.html>`_ An iOS compatible map application that supports custom maps
* `Osmtracker <osmtracker.html>`_ A program for recording GPS track logs, for use with OpenStreetMap traces or as a GPS log.
*  Coming soon QField. A portable version of QGIS for Android devices.

sUAS Data Collection & Analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `sUAS Flight Operations <flightops/index.html>`_ Flight prep, planning, execution, and post-flight procedures.
* `sUAS Data Extraction with QGIS <extraction_uas.html>`_ Extracting field data from sUAS imagery 
* `sUAS Data Analysis with QGIS <qgis_lab.html>`_ Extracting field data from sUAS imagery

.. toctree::
   :maxdepth: 1
   :hidden:

   kobo
   gaia
   mapsme
   osmtracker
   Flight Operations <flightops/index.rst>
   sUAS Data Extraction with QGIS <extraction_uas.md>
   sUAS Data Analysis with QGIS <qgis_lab.md>
