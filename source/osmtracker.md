# OSMTracker for Geographic Data Collection

Arielle Rose, Sierra Mabanta & Alex Mandel

[Geospatial and Farming Systems Research Consortium University of California, Davis](https://gfc.ucdavis.edu)

This guide helps you to:

1. Setup OSMTracker
1. Collect Data with OSMTracker on Android
 * Voice Record
 * Take Photo
    * Add Image from Gallery
1. Save a Track
1. Upload Data
  * Get Data on the Computer
  * Download Instructions for Windows
1. Import Data to QGIS
1. Appendix (TMI)
  * Display Track
  * Wayponts
     * Text Note
     * MISC
    * Restriction
    * Car
    * Tourism
    * Amenity & Amenity (+)
    * Way
    * Track
    * Landuse


What you'll need:
 * An Android tablet or phone and a computer
 * [OSMTracker](https://play.google.com/store/apps/details?id=net.osmtracker) for Android

**Pro Tip:** It may be useful if not necessary to you to download a [File Manager application](https://play.google.com/store/apps/details?id=com.asus.filemanager&hl=en_US).

## Setup

OSMTracker

This application does not have an account setup so you must record and export data from the same device.

1. Search for “OSMTracker” in the Play Store. Install and accept permissions.
![OSMTracker installation page](images/osmtracker/image19.png)
1. To begin, you will want to adjust some settings by selecting the menu button on the top right of the screen > settings.
 * Under the GPS section you can change the GPS Logging Interval.
The default is 0 second intervals. However, **10 seconds intervals is recommended** for a walking pace. This will help save battery and storage.
1. You can start tracking waypoints by selecting the “+” button (to the left of the menu button). It will start recording as soon as the GPS has a lock on the device location.
    * Select the back button to see the home screen. You can select and hold Track list to stop, resume, or upload tracking data
1. There are options to record notes or photos while tracking if you click the "Track Logger". If you would like to change the default settings on these options it is available through the menu button at the top right
    * Select the save icon to stop recording and save data to the archive.

![Long hold on track drop-down](images/osmtracker/image10.jpg)
![Actively recording track home screen](images/osmtracker/image7.png)

## Collect Data with OSMTracker *on Android*

To start a new track you will hit the plus button on the top right. ![new icon](images/osmtracker/image66.png) The next important step is to wait until the GPS location of the device has been triangulated. You will know that the location is being recorded when the GPS strength bars light up green. ![gps lock icon](images/osmtracker/image88.png) This may take a moment if you are in a remote location.

### Voice Recording

Voice Record - Will automatically record a 2 second audio clip. At anytime you can change the length of the recording
    + menu>settings>Voice record duration

![Voice recording](images/osmtracker/image47.png)

When this is image above is displaying it is in the process of recording audio input. Stop button will cancel the recording and delete what is recorded for that session.

### Take Photo

![take photo menu pop up](images/osmtracker/image32.png)
If you would like to take a new photo select “Take photo with camera”.

![Capture trigger button](images/osmtracker/image17.png) < Press this to capture that image
< Press this to toggle the front camera and the main/back camera

Here are the adjustments available to you through the OSMTracker camera mode. To open these mini-menus you select on the option you wish to change and the options will display. To close the mini-menu you can either tap the option you want to select or you can tap the image projection in the middle.

**Pro Tip:** The images taken through OSMTracker are usable but the highest possible quality. This may be due to optimization of the software for the hardware or for saving storage space, the reason is not known.

![Timer options](images/osmtracker/image46.png)

![flash options](images/osmtracker/image6.png)

![exposure options](images/osmtracker/image28.png)

**Pro Tip:** You can focus the image by tapping on the area want in focus before capturing.

#### Add Image from Gallery

The other option available to you is to take the photo through phone's optimized default camera app and select the photo from the gallery. If you would prefer to record the photo this way, you may want to ensure the photos are being **geotagged.**

First, select the phone’s settings menu. You will then open the menu option “Security and Location” and select “Location”.

![Security & Location](images/osmtracker/image21.png)

Next, ensure the location is toggled on. Then, you can adjust the accuracy under mode.

**Pro Tip:** Lower accuracy will help battery life on the device.

![Location toggle](images/osmtracker/image29.png)

![Location Mode](images/osmtracker/image35.png)

Now open the camera app on the device (your app may look different).
Open the menu options (top left corner for some devices).

Then open “settings”.

![settings menu](images/osmtracker/image30.png)

Toggle “save location” on. Now the images will be geotagged.

![save location toggle](images/osmtracker/image12.png)

## Save a Track

Great! There are two ways to **stop** collecting waypoints, you can either long-press the track from the list and select “stop tracking” or if you are in the track logger page you can select the save icon (floppy disk). ![save icon](images/osmtracker/image77.png)

![in-progress track long press menu](images/osmtracker/image99.png)

## Upload Data

To get to this menu we long-press a saved (not recording) track. Then select "Export as GPX"

![saved track long press menu](images/osmtracker/image36.png)

Another option if you have one or more tracks to export is to press the top right menu drop-down and select “Export all as GPX”, then select “Export all as GPX” on the warning screen.

![Export as GPX Menu](images/osmtracker/image48.png)

![Export all as GPX](images/osmtracker/image38.png)

Once it has been exported at a GPX a green indicator light icon will appear on the right side of the track.

Through the file manager application on the device you should see a folder has been created called “osmtracker”, this is where the data has been exported to.

**Pro Tip:**  [File Manager application](https://play.google.com/store/apps/details?id=com.asus.filemanager&hl=en_US)

### Get Data on the Computer

Plug the phone into the computer through the appropriate USB cable. You may be able to select the option to “Open device and view files” and have all of the files available to you. If this is not the case and the file explorer is blank there are a few more steps to do.

While the phone is still plugged into the computer, change the android system. Access this through the notification center by sliding down from the top of the screen.
Select “Android System - USB for file transfer”. Under “USB charging this device" select “Tap for more options.” Then select “Transfer files”. Give the device a moment to perform this task.

![notification center](images/osmtracker/image45.png)

![Android System](images/osmtracker/image41.png)

![Use USB to: Menu](images/osmtracker/image42.png)

Once the files have been transferred from the device to the computer this option menu should appear on the Windows computer. Select “Open device to view files”.

![AutoPlay menu](images/osmtracker/image40.png)

![File Explorer](images/osmtracker/image25.png)

This image above is viewed through the “Windows Explorer” ![File Explorer Icon](images/osmtracker/image8.png),you can access this at any time while the device is still connected to the computer.

![osmtracker folder](images/osmtracker/image9.png)

![GPS recording files](images/osmtracker/image31.png)

Select “Internal shared storage” to view the files. Select the folder “osmtracker” and from here you can download the files onto the computer for your convenience to use with a GIS program of your choosing.

### Download Instruction for Windows

To download these files onto the computer you can either highlight and then drag them (drag means to left trigger click the file with the mouse, HOLD down the trigger while moving the mouse) from the file manager window onto the computer desktop/on-top of a folder on the desktop. If you prefer to put the files into a location other than the desktop you can drag them over a menu option on the left hand side of the file manager window for example “Documents”.  If you want to expand any of the menu options to place then into a folder within that option you hover over the option while still dragging the folder, the folders available to that menu option will appear underneath. Let go of the mouse trigger to “drop a copy of that file in that desired location. You can also right click on the file and select “copy”, navigate to the desired location, right click in the desired location and select “paste”.

Once you have a copy of the track files, safely remove the device.

**Pro Tip:** *Safely Remove Device* go to the drop-down-menu and select “Android system USB for file transfer”, then “Tap for more options”, select “charge this device” and now you can unplug it safely from the computer.

## Import Data to QGIS

![gpx](images/osmtracker/image1.png)

Drag the GPX File to the QGIS layer window.

![Adding Vector layers to QGIS](images/osmtracker/image37.png)

Select “Select All” button and then select “OK”. You will then need to select the projection, for GSP it is almost always  EPSG: 4326 ![EPSG: 4326](images/osmtracker/image16.png) You can quickly change this at the button right hand corner of QGIS.

![vector data displayed in QGIS](images/osmtracker/image3.png)

You can add a basemap and much more!

![basemap](images/osmtracker/image23.png)

*The End.*

## *Appendix (TMI)*

### Display Track

This is a useful tool that can work with or without Internet connection. If you have Internet connection you can view the track with a basemap. Without Internet it will only display the track.

There are two ways to access this. If you are in the recording track you press the menu button on the top right and then select “display track”. To exit you can press the back button on the device.

![display track](images/osmtracker/image34.png)

You can also display the track if you are not in the progress of recording by long pressing the track. Then select “display”.

![display](images/osmtracker/image33.png)

Displaying track:

![map](images/osmtracker/image22.png)

#### Text Note

![text note](images/osmtracker/image27.png)

Tap on the blue line to pull up a keyboard function. Once you are done with the note hit “OK” and it will record the note with a geostamp. If you do not want to make a note select “cancel”.

### *WayPoints*

After you select a waypoint option it will change the lower ¾ of the options into quick location references. Tap the icon that seems most appropriate for your current location and it will add a geolocation to the track with the corresponding label. The following images show what options are available to you under that menu button.

#### MISC

![Misc](images/osmtracker/image43.png)

#### Restriction

![restriction](images/osmtracker/image13.png)

#### Car

![car](images/osmtracker/image39.png)

#### Tourism

![tourism](images/osmtracker/image15.png)

#### Amenity and Amenity (+)

![Amenity](images/osmtracker/image14.png)

![Amenity +](images/osmtracker/image26.png)

#### Way

![Way](images/osmtracker/image2.png)

#### Track

![track](images/osmtracker/image20.png)

#### Landuse

![land use](images/osmtracker/image18.png)
