# Guides

This is a collection of tutorials or guides for various software and data processing related to field data collection with tablets and sUAS(drones).

Homepage https://gfc.ucdavis.edu/guides

## Style Guides

* We use the Common Mark variant of Markdown, here is the [cheatsheet](https://commonmark.org/help/), please use the first option for style.
* Always use forward slash "/" in links and image references
* If you want something like and image, blockquote, etc.. inside of a list you need to indent with 4 spaces
* Use 1. for each item in a numbered list, the system will generate correct number

## Management

* Site is built using Sphinx with Markdown processed by recommonmark
* Build ```make html```
* Deploy ``` scp -r build/html/* username@data:/home/username/www-gfc/guides
